<?php
  class Curso extends CI_Model{
    public function __construct(){
      parent::__construct();
    }

    //funcion para insertar
    public function insertar($datos){
      return $this->db->insert('curso',$datos);
    }

    //funcion para consultar todos los Cursos
    public function consultarTodos(){

      // $listadoInstructores=$this->db->get('instructor');
      $this->db->join("instructor","instructor.id_ins=curso.fk_id_instructor");
            $listadoCursos=$this->db->get('curso');
      if ($listadoCursos->num_rows()>0) {
        //cuando si hay Cursos
        return $listadoCursos;

      }else{
        //cuando no hay Cursos
        return false;
      }
    }

    public function eliminar($id_cur){
      $this->db->where("id_cur",$id_cur);
      return $this->db->delete("curso");
    }

    public function actualizar($id_cur,$datos){
      $this->db->where('id_cur',$id_cur);
      return $this->db->update('curso',$datos);
    }

    public function consultarPorId($id_cur){
      $this->db->where('id_cur',$id_cur);
      $this->db->join("instructor","instructor.id_ins=curso.fk_id_instructor");
      $curso=$this->db->get('curso');
      if ($curso->num_rows()>0) {
        return $curso->row();
      }else{
        return false;
      }
    }



  }
 ?>
