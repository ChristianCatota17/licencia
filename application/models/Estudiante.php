<?php
  class Estudiante extends CI_Model{
    public function __construct(){
      parent::__construct();
    }

    //funcion para insertar
    public function insertar($datos){
      return $this->db->insert('estudiante',$datos);
    }

    //funcion para consultar todos los Estudiantes
    public function consultarTodos(){

      $listadoEstudiantes=$this->db->get('estudiante');
      if ($listadoEstudiantes->num_rows()>0) {
        //cuando si hay Estudiantes
        return $listadoEstudiantes;

      }else{
        //cuando no hay Estudiantes
        return false;
      }
    }

    public function eliminar($id_est){
      $this->db->where("id_est",$id_est);
      return $this->db->delete("estudiante");
    }

    public function actualizar($id_est,$datos){
      $this->db->where('id_est',$id_est);
      return $this->db->update('estudiante',$datos);
    }

    public function consultarPorId($id_est){
      $this->db->where('id_est',$id_est);
      $estudiante=$this->db->get('estudiante');
      if ($estudiante->num_rows()>0) {
        return $estudiante->row();
      }else{
        return false;
      }
    }



  }
 ?>
