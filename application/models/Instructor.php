<?php
  class Instructor extends CI_Model{
    public function __construct(){
      parent::__construct();
    }

    //funcion para insertar
    public function insertar($datos){
      return $this->db->insert('instructor',$datos);
    }

    //funcion para consultar todos los Instructores
    public function consultarTodos(){

      $listadoInstructores=$this->db->get('instructor');
      if ($listadoInstructores->num_rows()>0) {
        //cuando si hay Instructores
        return $listadoInstructores;

      }else{
        //cuando no hay Instructores
        return false;
      }
    }

    public function eliminar($id_ins){
      $this->db->where("id_ins",$id_ins);
      return $this->db->delete("instructor");
    }

    public function actualizar($id_ins,$datos){
      $this->db->where('id_ins',$id_ins);
      return $this->db->update('instructor',$datos);
    }

    public function consultarPorId($id_ins){
      $this->db->where('id_ins',$id_ins);
      $instructor=$this->db->get('instructor');
      if ($instructor->num_rows()>0) {
        return $instructor->row();
      }else{
        return false;
      }
    }



  }
 ?>
