<?php
      class Cursos extends CI_Controller{
        public function __construct(){
            parent::__construct();

            $this->load->model("curso");
            $this->load->model("instructor");
        }

        public function index(){
          $data["listadoCursos"]=$this->curso->consultarTodos();
          $this->load->view("header");
          $this->load->view("cursos/index",$data);
          $this->load->view("footer");
        }

        public function nuevo(){
          $data["listadoInstructores"]=$this->instructor->consultarTodos();
          $this->load->view("header");
          $this->load->view("cursos/nuevo",$data);
          $this->load->view("footer");
        }

        public function guardarCurso(){
            $datosNuevoCurso=array(
              "tipo_cur"=>$this->input->post("tipo_cur"),
              "inicio_cur"=>$this->input->post("inicio_cur"),
              "fin_cur"=>$this->input->post("fin_cur"),
              "lugar_cur"=>$this->input->post("lugar_cur"),
              "estado_cur"=>$this->input->post("estado_cur"),
              "fk_id_instructor"=>$this->input->post("fk_id_instructor")
            );


            if($this->curso->insertar($datosNuevoCurso)){
                $this->session->set_flashdata("confirmacion","Curso insertado exitosamente.");
            }else{
            }
            redirect("cursos/index");
        }

        public function procesarEliminacion($id_cur){
          if ($this->curso->eliminar($id_cur)) {
            $this->session->set_flashdata("confirmacion" ," curso eliminado exitosamente.");
          }else {
            $this->session->set_flashdata("confirmacion","Curso no eliminado.");
          }
          redirect("cursos/index");
        }

        public function editar($id_cur){
          $data["listadoInstructores"]=$this->instructor->consultarTodos();
          $data["curso"]=$this->curso->consultarPorId($id_cur);
          $this->load->view("header");
          $this->load->view("cursos/editar",$data);
          $this->load->view("footer");
        }

        public function procesarActualizacion(){
          $id_cur=$this->input->post("id_cur");

          $datosCursoEditado=array(
                "tipo_cur"=>$this->input->post("tipo_cur"),
                "inicio_cur"=>$this->input->post("inicio_cur"),
                "fin_cur"=>$this->input->post("fin_cur"),
                "lugar_cur"=>$this->input->post("lugar_cur"),
                "estado_cur"=>$this->input->post("estado_cur"),
                "fk_id_instructor"=>$this->input->post("fk_id_instructor")
          );
          if($this->curso->actualizar($id_cur,$datosCursoEditado)){
            $this->session->set_flashdata("confirmacion","Curso editado exitosamente.");

          }else{
            echo "error al editar";
          }
            redirect("cursos/index");
        }



        }
?>
