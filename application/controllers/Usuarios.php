<?php
      class Usuarios extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("usuario");

        }

        public function index(){
          $data["listadoUsuarios"]=$this->usuario->consultarTodos();
          $this->load->view("header");
          $this->load->view("usuarios/index",$data);
          $this->load->view("footer");
        }

        public function nuevo(){
          $this->load->view("header");
          $this->load->view("usuarios/nuevo");
          $this->load->view("footer");
        }

        public function guardarUsuario(){
            $datosNuevoUsuario=array(
                "nombre_usu"=>$this->input->post("nombre_usu"),
                "apellido_usu"=>$this->input->post("apellido_usu"),
                "password_usu"=>$this->input->post("password_usu"),
                "estado_usu"=>$this->input->post("estado_usu"),

                "email_usu"=>$this->input->post("email_usu")
            );


            if($this->usuario->insertar($datosNuevoUsuario)){
                $this->session->set_flashdata("confirmacion","Usuario insertado exitosamente.");
            }else{
            }
            redirect("usuarios/index");
        }

        public function procesarEliminacion($id_usu){
          if ($this->usuario->eliminar($id_usu)) {
            $this->session->set_flashdata("confirmacion" ," Usuario eliminado exitosamente.");
          }else {
            $this->session->set_flashdata("confirmacion","Usuario no eliminado.");
          }
          redirect("usuarios/index");
        }

        public function editar($id_usu){
          $data["usuario"]=$this->usuario->consultarPorId($id_usu);
          $this->load->view("header");
          $this->load->view("usuarios/editar",$data);
          $this->load->view("footer");
        }

        public function procesarActualizacion(){
          $id_usu=$this->input->post("id_usu");

          $datosUsuarioEditado=array(
            "nombre_usu"=>$this->input->post("nombre_usu"),
            "apellido_usu"=>$this->input->post("apellido_usu"),
            "password_usu"=>$this->input->post("password_usu"),
            "estado_usu"=>$this->input->post("estado_usu"),

            "email_usu"=>$this->input->post("email_usu")

          );
          if($this->usuario->actualizar($id_usu,$datosUsuarioEditado)){
            $this->session->set_flashdata("confirmacion","Usuario editado exitosamente.");

          }else{
            echo "error al editar";
          }
            redirect("usuarios/index");
        }



        }
?>
