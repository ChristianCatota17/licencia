<?php
      class Estudiantes extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("estudiante");

        }

        public function index(){
          $data["listadoEstudiantes"]=$this->estudiante->consultarTodos();
          $this->load->view("header");
          $this->load->view("estudiantes/index",$data);
          $this->load->view("footer");
        }

        public function nuevo(){
          $this->load->view("header");
          $this->load->view("estudiantes/nuevo");
          $this->load->view("footer");
        }

        public function guardarEstudiante(){
            $datosNuevoEstudiante=array(
                "nombre_est"=>$this->input->post("nombre_est"),
                "apellido_est"=>$this->input->post("apellido_est"),
                "fecha_nacimiento_est"=>$this->input->post("fecha_nacimiento_est"),
                "sexo_est"=>$this->input->post("sexo_est"),
                "cedula_est"=>$this->input->post("cedula_est"),
                "tipo_sangre_est"=>$this->input->post("tipo_sangre_est"),
                "direccion_est"=>$this->input->post("direccion_est"),
                "correo_est"=>$this->input->post("correo_est"),
                "password_est"=>$this->input->post("password_est"),
                "estado_est"=>$this->input->post("estado_est"),
                "restricciones_est"=>$this->input->post("restricciones_est"),
                "expedicion_est"=>$this->input->post("expedicion_est"),
                "expiracion_est"=>$this->input->post("expiracion_est"),
                "foto_est"=>$dataSubida["file_name"]
            );
            //Logica de Negocio necesaria para subir la FOTOGRAFIA del estudiante
              $this->load->library("upload");//carga de la libreria de subida de archivos
              $nombreTemporal="foto_estudiante_".time()."_".rand(1,5000);
              $config["file_name"]=$nombreTemporal;
              $config["upload_path"]=APPPATH.'../uploads/estudiantes/';
              $config["allowed_types"]="jpeg|jpg|png";
              $config["max_size"]=2*1024; //2MB
              $this->upload->initialize($config);
              //codigo para subir el archivo y guardar el nombre en la BDD
              if($this->upload->do_upload("foto_est")){
                $dataSubida=$this->upload->data();
                $datosNuevoEstudiante["foto_est"]=$dataSubida["file_name"];
              }

            if($this->estudiante->insertar($datosNuevoEstudiante)){
                $this->session->set_flashdata("confirmacion","Estudiante insertado exitosamente.");
            }else{
            }
            redirect("estudiantes/index");
        }

        public function procesarEliminacion($id_est){
          if ($this->estudiante->eliminar($id_est)) {
            $this->session->set_flashdata("confirmacion" ," estniversidad eliminado exitosamente.");
          }else {
            $this->session->set_flashdata("confirmacion","Estudiante no eliminado.");
          }
          redirect("estudiantes/index");
        }

        public function editar($id_est){
          $data["estudiante"]=$this->estudiante->consultarPorId($id_est);
          $this->load->view("header");
          $this->load->view("estudiantes/editar",$data);
          $this->load->view("footer");
        }

        public function procesarActualizacion(){
          $id_est=$this->input->post("id_est");
          $foto_est=$this->input->post('foto_est');
          //logica de negocio necesaria para subir la fotografia del estudiantes
          $this->load->library("upload"); //Carga de la libreria de subida de archivos

              if ($foto_est=="") {
                // code...
                $nombreTemporal="foto_estudiante_".time()."_".rand(1,5000); //creando un nombre aleatorio
                $config['file_name']=$nombre_aleatorio; //asignano el nombre al archivo subido
                $foto_est=$nombre_aleatorio;
              }else {
                unlink(  APPPATH.'../uploads/estudiantes/'.$foto_est);// si es true, llama la función
                $config["file_name"]=$foto_est; //asignano el nombre al archivo subido
              }
          $config["upload_path"]=APPPATH.'../uploads/estudiantes/'; //direccion de la carpeta para el guardado de las imgenes
          $config["allowed_types"]="jpeg|jpg|png"; //aqui va el formato de los archivos que deseamos cargar en este caso son imagenes si deseamos subir documento cambiamos
          $config["max_size"]=2*1024; //Tamaño maximo del archivo que deseamos que se suba en este cas esta 2MB
          $this->upload->initialize($config);

          if($this->upload->do_upload("foto_estu")){
            $dataSubida=$this->upload->data();
            $datosNuevoEstudiante["foto_estu"]=$dataSubida["file_name"];
          }
          $datosEstudianteEditado=array(
            "nombre_est"=>$this->input->post("nombre_est"),
            "apellido_est"=>$this->input->post("apellido_est"),
            "fecha_nacimiento_est"=>$this->input->post("fecha_nacimiento_est"),
            "sexo_est"=>$this->input->post("sexo_est"),
            "cedula_est"=>$this->input->post("cedula_est"),
            "tipo_sangre_est"=>$this->input->post("tipo_sangre_est"),
            "direccion_est"=>$this->input->post("direccion_est"),
            "correo_est"=>$this->input->post("correo_est"),
            "password_est"=>$this->input->post("password_est"),
            "estado_est"=>$this->input->post("estado_est"),
            "restricciones_est"=>$this->input->post("restricciones_est"),
            "expedicion_est"=>$this->input->post("expedicion_est"),
            "expiracion_est"=>$this->input->post("expiracion_est"),
            "foto_est"=>$dataSubida["file_name"]
          );
          if($this->estudiante->actualizar($id_est,$datosEstudianteEditado)){
            $this->session->set_flashdata("confirmacion","Estudiante editado exitosamente.");

          }else{
            echo "error al editar";
          }
            redirect("estudiantes/index");
        }

      

        }
?>
