<?php
      class Seguridades extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("usuario");
        }

        public function formularioLogin(){

          $this->load->view("seguridades/formularioLogin");

        }
        //funcion que valida las credenciales ingresadas
        public function validarAcceso(){
          $email_usu=$this->input->post("email_usu");
          $password_usu=$this->input->post("password_usu");
          $usuario=$this->usuario->buscarUsuarioPorEmailPassword($email_usu,$password_usu);
          if ($usuario) {
            // cuando el email y contraseña son correctas
            if ($usuario->estado_usu>0) {//validando estado
              // creando la variable de sesion con el nombre c0nectadoUTC
              $this->session->set_userdata("c0nectadoUTC",$usuario);
              $this->session->set_flashdata("bienvenido","Saludos, bienvenido al sistema");
              redirect("Welcome");//la primera vista que vera el usuario
            } else {
              $this->session->set_flashdata("error","Usuario bloqueado");
              redirect("seguridades/formularioLogin");
            }

          }else {//cuando no existe
            $this->session->set_flashdata("error","Email o contraseña incorrectos");
            redirect("seguridades/formularioLogin");
          }
        }

        public function cerrarSesion(){

          $this->session->sess_destroy();//matando la sesiones
          redirect("seguridades/formularioLogin");

        }


              }//cierre de la clase
        ?>
