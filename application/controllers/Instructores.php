<?php
      class Instructores extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("instructor");

        }

        public function index(){
          $data["listadoInstructores"]=$this->instructor->consultarTodos();
          $this->load->view("header");
          $this->load->view("instructores/index",$data);
          $this->load->view("footer");
        }

        public function nuevo(){
          $this->load->view("header");
          $this->load->view("instructores/nuevo");
          $this->load->view("footer");
        }

        public function guardarInstructor(){
            $datosNuevoInstructor=array(
              "nombre_ins"=>$this->input->post("nombre_ins"),
              "apellido_ins"=>$this->input->post("apellido_ins"),
              "especializacion_ins"=>$this->input->post("especializacion_ins"),
              "estado_ins"=>$this->input->post("estado_ins"),
              "foto_ins"=>$dataSubida["file_name"]
            );

            //Logica de Negocio necesaria para subir la FOTOGRAFIA del instructor
              $this->load->library("upload");//carga de la libreria de subida de archivos
              $nombreTemporal="foto_instructor_".time()."_".rand(1,5000);
              $config["file_name"]=$nombreTemporal;
              $config["upload_path"]=APPPATH.'../uploads/instructores/';
              $config["allowed_types"]="jpeg|jpg|png";
              $config["max_size"]=2*1024; //2MB
              $this->upload->initialize($config);
              //codigo para subir el archivo y guardar el nombre en la BDD
              if($this->upload->do_upload("foto_ins")){
                $dataSubida=$this->upload->data();
                $datosNuevoInstructor["foto_ins"]=$dataSubida["file_name"];
              }

            if($this->instructor->insertar($datosNuevoInstructor)){
                $this->session->set_flashdata("confirmacion","Instructor insertado exitosamente.");
            }else{
            }
            redirect("instructores/index");
        }

        public function procesarEliminacion($id_ins){
          if ($this->instructor->eliminar($id_ins)) {
            $this->session->set_flashdata("confirmacion" ," instructor eliminado exitosamente.");
          }else {
            $this->session->set_flashdata("confirmacion","Instructor no eliminado.");
          }
          redirect("instructores/index");
        }

        public function editar($id_ins){
          $data["instructor"]=$this->instructor->consultarPorId($id_ins);
          $this->load->view("header");
          $this->load->view("instructores/editar",$data);
          $this->load->view("footer");
        }

        public function procesarActualizacion(){
          $id_ins=$this->input->post("id_ins");

          $foto_ins=$this->input->post('foto_ins');
          //logica de negocio necesaria para subir la fotografia del instructores
          $this->load->library("upload"); //Carga de la libreria de subida de archivos

              if ($foto_ins=="") {
                // code...
                $nombreTemporal="foto_instructor_".time()."_".rand(1,5000); //creando un nombre aleatorio
                $config['file_name']=$nombre_aleatorio; //asignano el nombre al archivo subido
                $foto_ins=$nombre_aleatorio;
              }else {
                unlink(  APPPATH.'../uploads/instructores/'.$foto_ins);// si es true, llama la función
                $config["file_name"]=$foto_ins; //asignano el nombre al archivo subido
              }
          $config["upload_path"]=APPPATH.'../uploads/instructores/'; //direccion de la carpeta para el guardado de las imgenes
          $config["allowed_types"]="jpeg|jpg|png"; //aqui va el formato de los archivos que deseamos cargar en este caso son imagenes si deseamos subir documento cambiamos
          $config["max_size"]=2*1024; //Tamaño maximo del archivo que deseamos que se suba en este cas esta 2MB
          $this->upload->initialize($config);

          if($this->upload->do_upload("foto_inst")){
            $dataSubida=$this->upload->data();
            $datosNuevoInstructor["foto_inst"]=$dataSubida["file_name"];
          }

          $datosInstructorEditado=array(
            "nombre_ins"=>$this->input->post("nombre_ins"),
            "apellido_ins"=>$this->input->post("apellido_ins"),
            "especializacion_ins"=>$this->input->post("especializacion_ins"),
            "estado_ins"=>$this->input->post("estado_ins"),
            "foto_ins"=>$dataSubida["file_name"]
          );
          if($this->instructor->actualizar($id_ins,$datosInstructorEditado)){
            $this->session->set_flashdata("confirmacion","Instructor editado exitosamente.");

          }else{
            echo "error al editar";
          }
            redirect("instructores/index");
        }

        

        }
?>
