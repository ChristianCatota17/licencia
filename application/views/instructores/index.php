<br>
<center>
<h1>LISTADO DE INSTRUCTORES</h1>
<hr>
<br>
<center>

<a class="btn btn-primary" href="<?php echo site_url(); ?>/instructores/nuevo">Agregar nuevo</a>
<br><br><br>
<div class="row">
  <div class="col-md-1">

  </div>
  <div class="col-md-10">
    <?php if ($listadoInstructores): ?>

      <table class="table table-bordered table-hover table-striped">
        <thead>
          <tr>
            <th class="text-center">ID</th>
            <th class="text-center">NOMBRE</th>
            <th class="text-center">APELLIDO</th>
            <th class="text-center">ESPECIALIZACION</th>
            <th class="text-center">FOTO</th>
            <th class="text-center">ESTADO</th>
            <th class="text-center">OPCIONES</th>
          </tr>
        </thead>
        <tfoot>
          <?php foreach ($listadoInstructores->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                <?php echo $filaTemporal->id_ins; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->nombre_ins; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->apellido_ins; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->especializacion_ins; ?>
              </td>
              <td class="text-center">
                <?php if ($filaTemporal->foto_ins!=""): ?>
                  <img src="<?php echo base_url(); ?>/uploads/instructores/<?php echo $filaTemporal->foto_ins; ?>"
                  height="100px"
                  width="100px"
                  alt="">
                <?php else: ?>
                  N/A
                <?php endif; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->estado_ins; ?>
              </td>
              <td class="text-center">
                <a href="<?php echo site_url(); ?>/instructores/editar/<?php echo $filaTemporal->id_ins; ?>" class="btn btn-warning"><i class="fa fa-pen"></i></a>
                <a href="javascript:void(0)" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_ins; ?>');" class="btn btn-danger">
                  <i class="fa fa-trash"></i>
                </a>
              </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tfoot>
      </table>

    <?php else: ?>
      <div class="alert alert-danger">
        <h3 style="color:black">No se encontraron instructores</h3>
      </div>
    <?php endif; ?>
  </div>
  <div class="col-md-1">

  </div>
</div>


<script type="text/javascript">
    function confirmarEliminacion(id_ins){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el instructor de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/instructores/procesarEliminacion/"+id_ins;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
