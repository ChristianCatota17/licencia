<br><br><br>
<center>
<h1>LISTADO DE USUARIOS</h1>
<hr>
<br>
<center>

<a class="btn btn-primary" href="<?php echo site_url(); ?>/usuarios/nuevo">Agregar nuevo</a>
<br><br><br>
<div class="row">
  <div class="col-md-1">

  </div>
  <div class="col-md-10">
    <?php if ($listadoUsuarios): ?>

      <table class="table table-bordered table-hover table-striped">
        <thead>
          <tr>
            <th class="text-center">ID</th>
            <th class="text-center">NOMBRE</th>
            <th class="text-center">APELLIDO</th>
            <th class="text-center">EMAIL</th>
            <th class="text-center">PASSWORD</th>
            <th class="text-center">ESTADO</th>


            <th class="text-center">OPCIONES</th>
          </tr>
        </thead>
        <tfoot>
          <?php foreach ($listadoUsuarios->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                <?php echo $filaTemporal->id_usu; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->nombre_usu; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->apellido_usu; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->email_usu; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->password_usu; ?>
              </td>

              <td class="text-center">
                <?php echo $filaTemporal->estado_usu; ?>
              </td>
              <td class="text-center">
                <a href="<?php echo site_url(); ?>/usuarios/editar/<?php echo $filaTemporal->id_usu; ?>" class="btn btn-warning"><i class="fa fa-pen"></i></a>
                <a href="javascript:void(0)" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_usu; ?>');" class="btn btn-danger">
                  <i class="fa fa-trash"></i>
                </a>
              </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tfoot>
      </table>

    <?php else: ?>
      <div class="alert alert-danger">
        <h3>No de encontraron usuarios</h3>
      </div>
    <?php endif; ?>
  </div>
  <div class="col-md-1">

  </div>
</div>


<script type="text/javascript">
    function confirmarEliminacion(id_usu){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el usuario de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/usuarios/procesarEliminacion/"+id_usu;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
