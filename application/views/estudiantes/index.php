<br><br><br>
<center>
<h1>LISTADO DE ESTUDIANTES</h1>
<hr>
<br>
<center>

<a class="btn btn-primary" href="<?php echo site_url(); ?>/estudiantes/nuevo">Agregar nuevo</a>
<br><br><br>
<div class="row">

  <div class="col-md-12">
    <?php if ($listadoEstudiantes): ?>

      <table class="table table-bordered table-hover table-striped">
        <thead>
          <tr>
            <th class="text-center">ID</th>
            <th class="text-center">NOMBRE & APELLIDO</th>
            <th class="text-center">SEXO</th>
            <th class="text-center">CEDULA</th>
            <th class="text-center">DIRECCION</th>
            <th class="text-center">CORREO</th>

            <th class="text-center">ESTADO</th>
            <th class="text-center">RESTRICCION</th>
            <th class="text-center">FECHA DE EXPEDICION EXPIRACION</th>

            <th class="text-center">FOTO</th>

            <th class="text-center">OPCIONES</th>
          </tr>
        </thead>
        <tfoot>
          <?php foreach ($listadoEstudiantes->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                <?php echo $filaTemporal->id_est; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->nombre_est; ?>&nbsp;<?php echo $filaTemporal->apellido_est; ?>
              </td>


              <td class="text-center">
                <?php echo $filaTemporal->sexo_est; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->cedula_est; ?>
              </td>

              <td class="text-center">
                <?php echo $filaTemporal->direccion_est; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->correo_est; ?>
              </td>

              <td class="text-center">
                <?php echo $filaTemporal->estado_est; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->restricciones_est; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->expedicion_est; ?>&nbsp;-&nbsp;<?php echo $filaTemporal->expiracion_est; ?>
              </td>

              <td class="text-center">
                <?php if ($filaTemporal->foto_est!=""): ?>
                  <img src="<?php echo base_url(); ?>/uploads/estudiantes/<?php echo $filaTemporal->foto_est; ?>"
                  height="100px"
                  width="100px"
                  alt="">
                <?php else: ?>
                  N/A
                <?php endif; ?>
              </td>

              <td class="text-center">
                <a href="<?php echo site_url(); ?>/estudiantes/editar/<?php echo $filaTemporal->id_est; ?>" class="btn btn-warning"><i class="fa fa-pen"></i></a>
                <a href="javascript:void(0)" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_est; ?>');" class="btn btn-danger">
                  <i class="fa fa-trash"></i>
                </a>
              </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tfoot>
      </table>

    <?php else: ?>
      <div class="alert alert-danger">
        <h3>No de encontraron estudiantes</h3>
      </div>
    <?php endif; ?>
  </div>
  
</div>


<script type="text/javascript">
    function confirmarEliminacion(id_est){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el estudiante de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/estudiantes/procesarEliminacion/"+id_est;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
