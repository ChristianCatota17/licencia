<br>
<center>
<h1>LISTADO DE CURSOS</h1>
<hr>
<br>
<center>

<a class="btn btn-primary" href="<?php echo site_url(); ?>/cursos/nuevo">Agregar nuevo</a>
<br><br><br>
<div class="row">
  <div class="col-md-1">

  </div>
  <div class="col-md-10">
    <?php if ($listadoCursos): ?>

      <table class="table table-bordered table-hover table-striped">
        <thead>
          <tr>
            <th class="text-center">ID</th>
            <th class="text-center">TIPO</th>
            <th class="text-center">INICIO</th>
            <th class="text-center">FIN</th>
            <th class="text-center">LUGAR</th>
            <th class="text-center">ESTADO</th>
            <th class="text-center">INSTRUCTOR</th>
            <th class="text-center">OPCIONES</th>
          </tr>
        </thead>
        <tfoot>
          <?php foreach ($listadoCursos->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                <?php echo $filaTemporal->id_cur; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->tipo_cur; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->inicio_cur; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->fin_cur; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->lugar_cur; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->estado_cur; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->nombre_ins; ?>
              </td>
              <td class="text-center">
                <a href="<?php echo site_url(); ?>/cursos/editar/<?php echo $filaTemporal->id_cur; ?>" class="btn btn-warning"><i class="fa fa-pen"></i></a>
                <a href="javascript:void(0)" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_cur; ?>');" class="btn btn-danger">
                  <i class="fa fa-trash"></i>
                </a>
              </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tfoot>
      </table>

    <?php else: ?>
      <div class="alert alert-danger">
        <h3 style="color:black">No se encontraron cursos</h3>
      </div>
    <?php endif; ?>
  </div>
  <div class="col-md-1">

  </div>
</div>


<script type="text/javascript">
    function confirmarEliminacion(id_cur){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el curso de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/cursos/procesarEliminacion/"+id_cur;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
