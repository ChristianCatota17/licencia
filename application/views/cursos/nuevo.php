<br><br>
<div class="row">
  <div class="col-md-4">

  </div>
  <div class="col-md-4">


<form class="" action="<?php echo site_url(); ?>/cursos/guardarCurso" method="post" enctype="multipart/form-data">
  <label for="">TIPO DE LICENCIA: </label><br>
  <input class="form-control" type="text" name="tipo_cur" id="tipo_cur" value="" placeholder="Por favor ingrese el tipo de curso"><br>
  <label for="">INICIO DE CURSO: </label><br>
  <input class="form-control" type="date" name="inicio_cur" id="inicio_cur" value="" placeholder="Por favor ingrese la fecha de inicio del curso"><br>
  <label for="">FIN DE CURSO: </label><br>
  <input class="form-control" type="date" name="fin_cur" id="fin_cur" value="" placeholder="Por favor ingrese la fecha de fin de curso"><br>
  <label for="">DIRECCION DEL CURSO: </label><br>
  <input class="form-control" type="text" name="lugar_cur" id="lugar_cur" value="" placeholder="Por favor ingrese la direccion del curso"><br>
  <label for="">ESTADO DEL CURSO: </label><br>
  <input class="form-control" type="text" name="estado_cur" id="estado_cur" value="" placeholder="Por favor seleccione el estado del curso"><br>

  <label for="">INSTRUCTOR</label>
  <select class="form-control" name="fk_id_instructor" id="fk_id_instructor" >
    <option value="">--Seleccione un instructor--</option>
    <?php if ($listadoInstructores): ?>
      <?php foreach ($listadoInstructores->result() as $instructorTemporal): ?>
        <option value="<?php echo $instructorTemporal->id_ins; ?>">
          <?php echo $instructorTemporal->nombre_ins; ?>
        </option>

      <?php endforeach; ?>

    <?php endif; ?>

  </select>

  <br>
  <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>
  &nbsp;&nbsp;&nbsp;

  <a href="<?php echo site_url(); ?>/cursos/index" class="btn btn-warning">CANCELAR</a>
</form>
</div>
<div class="col-md-4">

</div>
</div>
